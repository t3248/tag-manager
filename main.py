import tkinter as tk
from pathlib import Path as path

def tree(directory):
    print(f'+ {directory}')
    for path in sorted(directory.rglob('*')):
        depth = len(path.relative_to(directory).parts)
        spacer = '    ' * depth
        print(f'{spacer}+ {path.name}')


# root configuration
root = tk.Tk()
root.title('tag manager')
root.geometry('640x480')

root.rowconfigure(0, weight=1)
root.rowconfigure(1, weight=8)

root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=4)


# tool bar configuration
tool_bar = tk.Frame(root,)
tool_bar.grid(sticky=tk.N + tk.E + tk.S + tk.W, columnspan=2)

tool_bar_CF = tk.Label(tool_bar, bg='black')
tool_bar_CF.pack(fill="both", expand=True)


# side bar configuration
side_bar = tk.Frame(root, bg='red')
side_bar.grid(row=1, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
side_bar.columnconfigure(0, weight=1)
side_bar.rowconfigure(0, weight=1)

# tags
fake_place = ['eirik', 'desktop', 'documents', 'downloads', 'music', 'games']
places = tk.LabelFrame(side_bar, text="Places", bg='green')
places.grid(row=0, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
places_info = tk.Listbox(places)

for x in fake_place:
    places_info.insert(tk.END, x)

places_info.pack(fill="both", expand=True)


# devises
devices = tk.LabelFrame(side_bar, text='Devices', bg='blue', pady=5)
devices.grid(row=1, column=0, sticky=tk.N + tk.E + tk.S + tk.W)
devices_info = tk.Listbox(places)

for x in fake_place:
    devices_info.insert(tk.END, x)

devices_info.pack(fill="both", expand=True)


# file screen configuration
file_screen = tk.Frame(root,)
file_screen.rowconfigure(0, weight=1)
file_screen.columnconfigure(0, weight=1)
file_screen.grid(row=1, column=1, sticky=tk.N + tk.E + tk.S + tk.W)


files = tk.Label(file_screen, bg='white', text=f'{tree(path.cwd())}')
files.grid(sticky=tk.N + tk.E + tk.S + tk.W)

file_status = tk.Label(file_screen, bg='gray92')
file_status.grid(sticky=tk.N + tk.E + tk.S + tk.W)


root.mainloop()
